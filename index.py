import tkinter as tk
from tkinter import *
from tkinter import ttk

import sqlite3


class Product:
    db_name = 'database.db'

    def __init__(self, window):
        self.wind = window
        self.wind.title('Product Application')

        # crear frame
        frame = LabelFrame(self.wind, text='Register a new Product')
        frame.grid(row=0, column=0, columnspan=3, pady=20)

        # name input
        Label(frame, text='Name: ').grid(row=1, column=0)
        self.name = Entry(frame)
        self.name.focus()
        self.name.grid(row=1, column=1)

        # price input
        Label(frame, text='Price: ').grid(row=2, column=0)
        self.price = Entry(frame)
        self.price.grid(row=2, column=1)

        # buttom add product
        tk.Button(frame, text='Save Product', command=self.addProduct).grid(row=3, columnspan=2, sticky=W + E)

        # output messages
        self.message = Label(text='', fg='red')
        self.message.grid(row=3, column=0, columnspan=2, sticky=W + E)

        # table
        self.tree = ttk.Treeview(height=10, columns=2)
        self.tree.grid(row=4, column=0, columnspan=2)
        self.tree.heading('#0', text='Name', anchor=CENTER)
        self.tree.heading('#1', text='Price', anchor=CENTER)

        # Buttons
        ttk.Button(text='DELETE', command=self.deleteProduct).grid(row=5, column=0, sticky=W + E)
        ttk.Button(text='UPDATE', command=self.editProduct).grid(row=5, column=1, sticky=W + E)

        # filling rows
        self.getProduct()

    def run_query(self, query, params=()):
        print(params)
        with sqlite3.connect(self.db_name) as conn:
            cursor = conn.cursor()
            result = cursor.execute(query, params)
            conn.commit()
            return result

    def getProduct(self):
        records = self.tree.get_children()
        # limpiar lista
        for element in records:
            self.tree.delete(element)
        query = 'SELECT * FROM product ORDER BY name DESC'
        db_rows = self.run_query(query)
        # añadir nuevos datos
        for row in db_rows:
            self.tree.insert('', 0, text=row[1], values=row[2])

    def validation(self):
        return len(self.name.get()) != 0 and len(self.price.get()) != 0

    def addProduct(self):
        if self.validation():
            query = 'INSERT INTO product VALUES (NULL, ? , ?)'
            params = (self.name.get(), self.price.get())
            self.run_query(query, params)
            self.message['text'] = 'Product {} added Successfully'.format(self.name.get())
            self.name.delete(0, END)
            self.price.delete(0, END)
        else:
            self.message['text'] = 'Name and Price are required!'
        self.getProduct()

    def deleteProduct(self):
        self.message['text'] = ''
        try:
            self.tree.item(self.tree.selection())['text'][0]
        except IndexError as e:
            self.message['text'] = 'Please select a Record'
            return
        name = self.tree.item(self.tree.selection())['text']
        query = 'DELETE FROM product WHERE name = ?'
        self.run_query(query, (name,))
        self.message['text'] = 'Record {} deleted Successfully'.format(name)
        self.getProduct()

    def editProduct(self):
        self.message['text'] = ''
        try:
            self.tree.item(self.tree.selection())['text'][0]
        except IndexError as e:
            self.message['text'] = 'Please select a Record'
            return
        name = self.tree.item(self.tree.selection())['text']
        old_price = self.tree.item(self.tree.selection())['values'][0]
        self.edit_wind = Toplevel()
        self.edit_wind.geometry("250x150")
        self.edit_wind.title = 'Edit Product'

        # old name
        Label(self.edit_wind, text='Old Name').grid(row=0, column=1)
        Entry(self.edit_wind, textvariable=StringVar(self.edit_wind, value=name), state='readonly').grid(row=0,
                                                                                                         column=2)

        # new name
        Label(self.edit_wind, text='New Name').grid(row=1, column=1)
        new_name = Entry(self.edit_wind)
        new_name.grid(row=1, column=2)

        # old price
        Label(self.edit_wind, text='Old Price').grid(row=2, columns=2)
        Entry(self.edit_wind, textvariable=StringVar(self.edit_wind, value=old_price), state='readonly').grid(row=2,
                                                                                                              column=2)
        # new price
        Label(self.edit_wind, text='New Price').grid(row=3, column=1)
        new_price = Entry(self.edit_wind)
        new_price.grid(row=3, column=2)

        # button
        Button(self.edit_wind, text='UPDATE',
               command=lambda: self.edit_record(new_name.get(), name, new_price.get(), old_price)).grid(row=4, column=2,
                                                                                                        sticky=W)

    def edit_record(self, new_name, name, new_price, old_price):
        query = 'UPDATE product SET name = ?, price = ? WHERE name = ? AND price = ?'
        params = (new_name, new_price, name, old_price)

        self.run_query(query, params)
        self.edit_wind.destroy()
        self.getProduct()
        self.message['text'] = 'Record {} has been updated successfully'.format(name)

    def center(win):
        win.update_idletasks()
        width = win.winfo_width()
        height = win.winfo_height()
        x = (win.winfo_screenwidth() // 2) - (width // 2)
        y = (win.winfo_screenheight() // 2) - (height // 2)
        win.geometry('{}x{}+{}+{}'.format(width, height, x, y))

if __name__ == '__main__':
    window = tk.Tk()
    window.geometry("400x400")


    def center_window(w=400, h=400):
        # get screen width and height
        ws = window.winfo_screenwidth()
        hs = window.winfo_screenheight()
        # calculate position x, y
        x = (ws / 2) - (w / 2)
        y = (hs / 2) - (h / 2)
        window.geometry('%dx%d+%d+%d' % (w, h, x, y))


    center_window(400, 400)
    aplicattion = Product(window)


    window.mainloop()
